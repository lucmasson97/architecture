package asynchronousMessage;

import java.net.URI;
import java.net.URISyntaxException;
 
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
 
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;

public class JmsMessageAsynchronousQueueImplement {
	public static void main(String[] args) throws URISyntaxException, Exception {
        BrokerService broker = BrokerFactory.createBroker(new URI(
                "broker:(tcp://localhost:61616)"));
        broker.start();
        Connection connection = null;
        try {
            
            ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                    "tcp://localhost:61616");
            connection = connectionFactory.createConnection();
            Session session = connection.createSession(false,
                    Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue("customerQueue");
            
            String payload = "Important Task";
            Message msg = session.createTextMessage(payload);
            
            // Producer
            MessageProducer producer = session.createProducer(queue);
            System.out.println("##############");
            System.out.println("Sending text '" + payload + "'");
            System.out.println("##############");
            producer.send(msg);
 
            // Consumer
            MessageConsumer consumer = session.createConsumer(queue);
            consumer.setMessageListener(new MessageListenerImplement("ConsumerWithCustomMessage"));
            connection.start();
            session.close();
            
        } finally {
            if (connection != null) {
                connection.close();
            }
            broker.stop();
        }
    }
 
}
