package asynchronousMessage;

import javax.jms.MessageListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

public class MessageListenerImplement implements MessageListener {
	
    private String consumerName;
    
    public MessageListenerImplement(String consumerName) {
        this.consumerName = consumerName;
    }
 
    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        try {
        	System.out.println("##############");
            System.out.println(consumerName + " received " + textMessage.getText());
            System.out.println("##############");
        } catch (JMSException e) {          
            e.printStackTrace();
        }
    }
 
}
